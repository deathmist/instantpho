# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-20 14:57-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../src/app/qml/components/BusyIndicator.qml:53
msgid "Processing image..."
msgstr ""

#: ../src/app/qml/components/ClaritySettingsPanel.qml:55
#: ../src/app/qml/components/ClaritySettingsPopover.qml:53
#: ../src/app/qml/components/FiltersView.qml:180
#: ../src/app/qml/components/OtherActionsView.qml:103
msgid "Reset"
msgstr ""

#: ../src/app/qml/components/ClaritySettingsPanel.qml:74
#: ../src/app/qml/components/ClaritySettingsPopover.qml:72
#: ../src/app/qml/components/FiltersView.qml:199
#: ../src/app/qml/components/OtherActionsView.qml:123
msgid "OK"
msgstr ""

#: ../src/app/qml/components/FilterSidebarPage.qml:29
#: ../src/app/qml/ui/FiltersPage.qml:133
msgid "Filters"
msgstr ""

#: ../src/app/qml/components/FilterSidebarPage.qml:29
#: ../src/app/qml/ui/FiltersPage.qml:133
msgid "Effects"
msgstr ""

#: ../src/app/qml/components/FilterSidebarPage.qml:201
msgid "Filter opacity level:"
msgstr ""

#: ../src/app/qml/components/TextualButtonStyle.qml:28
#: ../src/app/qml/components/TextualButtonStyle.qml:36
#: ../src/app/qml/components/TextualButtonStyle.qml:43
msgid "Pick"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:23
msgid "Adjust"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:34
msgid "Brightness"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:45
msgid "Contrast"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:56
msgid "Saturation"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:67
msgid "Temperature"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:78
msgid "Highlights"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:89
msgid "Shadows"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:100
msgid "Vignette"
msgstr ""

#: ../src/app/qml/effects/EffectsList.qml:111
msgid "Sharpen"
msgstr ""

#: ../src/app/qml/ui/AboutPage.qml:27
msgid "About"
msgstr ""

#. TRANSLATORS: Version of the software (e.g. "Version 0.3.51")
#: ../src/app/qml/ui/AboutPage.qml:62
msgid "Version %1"
msgstr ""

#: ../src/app/qml/ui/AboutPage.qml:77
msgid "Released under the terms of the GNU GPL v3"
msgstr ""

#: ../src/app/qml/ui/AboutPage.qml:88
msgid "Source code available on %1"
msgstr ""

#: ../src/app/qml/ui/AboutPage.qml:110
msgid "Report a bug"
msgstr ""

#: ../src/app/qml/ui/CropPage.qml:35
msgid "Original"
msgstr ""

#: ../src/app/qml/ui/CropPage.qml:169
msgid "Cancel"
msgstr ""

#: ../src/app/qml/ui/CropPage.qml:183
msgid "Next"
msgstr ""

#: ../src/app/qml/ui/FiltersPage.qml:62
msgid "Back"
msgstr ""

#: ../src/app/qml/ui/FiltersPage.qml:73
msgid "Done"
msgstr ""

#: ../src/app/qml/ui/ImportPage.qml:28
msgid "Choose from"
msgstr ""

#: ../src/app/qml/ui/MainPage.qml:87
msgid "An application for Ubuntu devices."
msgstr ""

#: ../src/app/qml/ui/MainPage.qml:100
msgid "Import a photo from..."
msgstr ""

#: ../src/app/qml/ui/MainPage.qml:120
msgid "About this application"
msgstr ""

#: ../data/instantpho.desktop.in.in.h:1
msgid ""
"instant;fx;image;filter;photo;picture;pic;manipulation;effect;edit;editor;"
"editing"
msgstr ""

#: ../data/instantpho.desktop.in.in.h:2
msgid "Photo editing app for Ubuntu devices."
msgstr ""
