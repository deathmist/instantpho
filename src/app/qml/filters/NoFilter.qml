import QtQuick 2.4
import InstantFX.ImageProcessor 0.1

FilterBase {
    id: rootItem

    ShaderEffectSource {
        anchors.fill: parent
        sourceItem: rootItem.img
    }
}
