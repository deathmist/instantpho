/*
 * Copyright (C) 2015 Roman Shchekin
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4

PinchArea {
    id: pinchArea

    property Flickable targetFlickable: null
    property real totalScale: 1

    property real minimumZoom: 0.5
    property real maximumZoom: 4.0
    property alias zoomValue: zoomHelper.scale

    function pinchUpdatedHandler(pinch) {
        if (zoomHelper.scale < pinchArea.maximumZoom &&
                zoomHelper.scale > pinchArea.minimumZoom) {
            targetFlickable.scale = pinch.scale
        }
    }

    function pinchFinishedHandler() {
        var pt = pinchArea.mapFromItem(targetFlickable, -targetFlickable.contentX , -targetFlickable.contentY )
        // console.log("pinchFinishedHandler", -myItem.contentX, -myItem.contentY, Math.round(pt.x), Math.round(pt.y))

        totalScale = zoomHelper.scale
        targetFlickable.scale = 1

        // Overwrite contentX and contentY values.
        // This is required since a change in contentWidth or contentHeight causes
        // the Flickable to reset the position of the its content.

        if (targetFlickable.contentWidth > targetFlickable.width)
            targetFlickable.contentX = -pt.x

        if (targetFlickable.contentHeight > targetFlickable.height)
            targetFlickable.contentY = -pt.y

        // Return to the legal bounds
        targetFlickable.returnToBounds()
    }

    pinch {
        target: Item { id: zoomHelper }
        minimumScale: pinchArea.minimumZoom
        maximumScale: pinchArea.maximumZoom
    }

    onPinchStarted: {
        targetFlickable.interactive = false
    }

    onPinchUpdated: {
        pinchUpdatedHandler(pinch)
    }

    onPinchFinished: {
        targetFlickable.interactive = true
        pinchFinishedHandler()
    }
}
