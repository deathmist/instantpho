/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

#include "procdeviceinfo.h"

#include <QtWidgets/QApplication>
#include <QScreen>
#include <qmath.h>
#include <QDebug>

ProcDeviceInfo::ProcDeviceInfo(QObject *parent)
    : QObject(parent)
{
    bool isAndroid = bool(getenv("ANDROID_ROOT") != 0);

    if (!isAndroid) {
        // Use a default safe size for PCs.
        m_optimalPreviewSize = 1080;

        return;
    }

    // Heuristic!
    // optimalSize = sqrt(screenWidth * screenHeight / 2)
    //
    // We anyway get interesting results from this formula:
    // - Aquaris E5 (720*1280) -----------> ~680px
    // - Nexus 5 (1080*1920) -------------> ~1018px
    // - Samsung Galaxy S7 (1440*2560) ---> ~1358px

    QScreen *screen = QApplication::screens().at(0);

    int area = screen->size().width() * screen->size().height();

    int s = int(qSqrt(area * 0.5));
    s = (s % 2 == 0) ? s : (s + 1);

    qDebug() << "Optimal size for image preview is" << s;

    m_optimalPreviewSize = s;
}
