#include <QCoreApplication>
#include <QImage>
#include <QDebug>

#include <QtCore/qmath.h>

// Temperature color algorithm based on:
// https://android.googlesource.com/platform/system/media/+/8f310822ddf1205ef990d21c012eaa9530148942/mca/filterpacks/imageproc/java/ColorTemperatureFilter.java
//
/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QImage img(256, 99, QImage::Format_RGB32);

    for (int x=0; x<256; ++x) {
        for (int y=0; y<99; ++y) {
            qreal adj = qreal(y);
            adj /= 100;

            adj *= 0.5;
            adj -= 0.25;

            qreal tmp = qreal(x);
            tmp /= 255.0;

            qreal red = tmp + tmp * (1.0 - tmp) * adj;
            qreal blu = red;

            qreal green;

            if (adj > 0)
                green = tmp + tmp * (1.0 -tmp) * adj * 0.5;
            else
                green = tmp;

            red *= 255.0;
            blu *= 255.0;
            green *= 255.0;


            int newRed = qBound(0, int(red), 255);
            int newGreen = qBound(0, int(green), 255);
            int newBlue = qBound(0, int(blu), 255);

            QRgb value = qRgb(newRed, newGreen, newBlue);

            qDebug() << tmp << adj << red << green << blu << newRed << newGreen << newBlue << value;
            img.setPixel(x,y, value);
        }
    }

    qDebug() << "Map saved?" << img.save("/tmp/colorTemperatureMap.png", "PNG", 100);

    return a.exec();
}

